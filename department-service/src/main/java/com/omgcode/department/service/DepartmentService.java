package com.omgcode.department.service;

import com.omgcode.department.entity.Department;
import com.omgcode.department.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public Department saveDepartment(Department department) {
        log.info("Inside the method saveDepartment of service department");
        return departmentRepository.save(department);
    }

    public Optional<Department> findDepartmentById(Long departmentId) {
        log.info("Inside method findDepartmentById of department service");

        List<Department> departments = new ArrayList<Department>();
        return Optional.ofNullable(departmentRepository.findByDepartmentId(departmentId));
    }
}
