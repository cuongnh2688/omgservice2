package com.omgcode.department.repository;

import com.omgcode.department.entity.Department;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    Department findByDepartmentId(Long departmentId);

    List<Department> findByIdIn(List<Integer> ids, Sort sort);
}
