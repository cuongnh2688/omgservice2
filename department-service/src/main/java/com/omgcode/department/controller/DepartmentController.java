package com.omgcode.department.controller;

import com.omgcode.department.entity.Department;
import com.omgcode.department.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/departments")
@Slf4j
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @PostMapping("/")
    public Department saveDepartment(@RequestBody Department department) {
        log.info("Inside the controller department");
        return departmentService.saveDepartment(department);
    }

    @GetMapping("/{departmentId}")
    public Optional<Department> findDepartmentById(@PathVariable Long departmentId) {
        return departmentService.findDepartmentById(departmentId);
    }
}
